#!/bin/sh

grep '"api_key": ""' request.json && { echo "Need to fill in api_key in request.json"; exit 1; }
curl --silent https://api.dev.testing-farm.io/v0.1/requests --header 'Content-Type: application/json' --data @request.json --output response.json
