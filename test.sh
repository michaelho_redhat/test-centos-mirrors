#!/bin/bash

set -euxo pipefail

sudo dnf install -y curl

#ip -6 addr
#curl --verbose -g -6 'http://[2607:f7c0:1:14c0:c0f0:a600:0:a5]:80/'

for ((i = 1; i <= 250; i++)); do
  curl --verbose -H "Connection: close" 'http://mirror.stream.centos.org/9-stream/BaseOS/aarch64/os/repodata/repomd.xml' -o repomd.xml
  sleep 10
done
